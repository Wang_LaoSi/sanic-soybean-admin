import { request } from '../request';

/**
 * Get captcha image
 *
 * @param captcha_type The type of captcha to get
 */
export function fetchCaptcha(captcha_type: number = 1) {
  return request<{
    captcha_id: string;
    captcha_image: string;
  }>({
    url: '/api/v1/auth/auth/captcha',
    method: 'get',
    params: { captcha_type } // 1: gif, 2: png
  });
}
