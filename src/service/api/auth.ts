import { request } from '../request';

/**
 * Login
 *
 * @param params Login parameters
 */
export function fetchLogin(params: {
  account: string;
  provider: string;
  password: string;
  code: string;
  captcha_id: string;
  captcha_code: string;
}) {
  return request<Api.Auth.LoginToken>({
    url: '/api/v1/auth/auth/login',
    method: 'post',
    data: params
  });
}

/** Get user info */
export function fetchGetUserInfo() {
  return request<Api.Auth.UserInfo>({ url: '/api/v1/auth/auth/user-info' });
}

/**
 * Refresh token
 *
 * @param refreshToken Refresh token
 */
export function fetchRefreshToken(refreshToken: string) {
  return request<Api.Auth.LoginToken>({
    url: '/auth/refreshToken',
    method: 'post',
    data: {
      refreshToken
    }
  });
}

/**
 * return custom backend error
 *
 * @param code error code
 * @param msg error message
 */
export function fetchCustomBackendError(code: string, msg: string) {
  return request({ url: '/auth/error', params: { code, msg } });
}
