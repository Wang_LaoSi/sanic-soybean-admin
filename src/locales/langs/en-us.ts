const local: App.I18n.Schema = {
  system: {
    title: 'Soybean Admin System',
    updateTitle: 'System Version Update Notification',
    updateContent: 'A new system version is available. Would you like to refresh the page now?',
    updateConfirm: 'Refresh Now',
    updateCancel: 'Later'
  },
  common: {
    action: 'Action',
    add: 'Add',
    addSuccess: 'Added Successfully',
    backToHome: 'Back to Home',
    batchDelete: 'Batch Delete',
    cancel: 'Cancel',
    close: 'Close',
    check: 'Check',
    expandColumn: 'Expand Column',
    columnSetting: 'Column Settings',
    config: 'Configuration',
    confirm: 'Confirm',
    delete: 'Delete',
    deleteSuccess: 'Deleted Successfully',
    confirmDelete: 'Are you sure you want to delete?',
    edit: 'Edit',
    warning: 'Warning',
    error: 'Error',
    index: 'Index',
    keywordSearch: 'Please enter keywords to search',
    logout: 'Logout',
    logoutConfirm: 'Are you sure you want to logout?',
    lookForward: 'Stay Tuned',
    modify: 'Modify',
    modifySuccess: 'Modified Successfully',
    noData: 'No Data',
    operate: 'Operation',
    pleaseCheckValue: 'Please check if the input value is valid',
    refresh: 'Refresh',
    reset: 'Reset',
    search: 'Search',
    switch: 'Switch',
    tip: 'Tip',
    trigger: 'Trigger',
    update: 'Update',
    updateSuccess: 'Updated Successfully',
    userCenter: 'User Center',
    yesOrNo: {
      yes: 'Yes',
      no: 'No'
    },
    email: 'Email',
    verifyCode: 'Verification Code',
    sendCode: 'Send Code',
    newPassword: 'New Password',
    confirmPassword: 'Confirm Password',
    required: 'Required',
    invalidEmail: 'Invalid email format',
    verifyCodeLength: 'Verification code must be 6 digits',
    passwordLength: 'Password must be at least 6 characters',
    passwordNotMatch: 'Passwords do not match',
    next: 'Next',
    back: 'Back',
    submit: 'Submit',
    resend: 'Resend',
    noCode: "Haven't received code",
    validateSuccess: 'Validation Successful',
    welcomeBack: 'Welcome back, {userName}!',
    captchaPlaceholder: 'Please enter captcha code'
  },
  request: {
    logout: 'Logout after request failure',
    logoutMsg: 'User session expired, please login again',
    logoutWithModal: 'Show modal before logout after request failure',
    logoutWithModalMsg: 'User session expired, please login again',
    refreshToken: 'Token expired, refreshing token',
    tokenExpired: 'Token expired'
  },
  theme: {
    themeSchema: {
      title: 'Theme Mode',
      light: 'Light Mode',
      dark: 'Dark Mode',
      auto: 'Follow System'
    },
    grayscale: 'Grayscale Mode',
    colourWeakness: 'Color Weakness Mode',
    layoutMode: {
      title: 'Layout Mode',
      vertical: 'Left Menu Mode',
      'vertical-mix': 'Left Menu Mixed Mode',
      horizontal: 'Top Menu Mode',
      'horizontal-mix': 'Top Menu Mixed Mode',
      reverseHorizontalMix: 'Reverse Primary and Sub Menu Position'
    },
    recommendColor: 'Recommended Algorithm Colors',
    recommendColorDesc: 'Color recommendation algorithm reference',
    themeColor: {
      title: 'Theme Color',
      primary: 'Primary',
      info: 'Info',
      success: 'Success',
      warning: 'Warning',
      error: 'Error',
      followPrimary: 'Follow Primary'
    },
    scrollMode: {
      title: 'Scroll Mode',
      wrapper: 'Outer Scroll',
      content: 'Content Scroll'
    },
    page: {
      animate: 'Page Transition Animation',
      mode: {
        title: 'Page Transition Animation Type',
        'fade-slide': 'Slide',
        fade: 'Fade',
        'fade-bottom': 'Fade Bottom',
        'fade-scale': 'Fade Scale',
        'zoom-fade': 'Zoom Fade',
        'zoom-out': 'Zoom Out',
        none: 'None'
      }
    },
    fixedHeaderAndTab: 'Fixed Header and Tab Bar',
    header: {
      height: 'Header Height',
      breadcrumb: {
        visible: 'Show Breadcrumb',
        showIcon: 'Show Breadcrumb Icon'
      }
    },
    tab: {
      visible: 'Show Tab Bar',
      cache: 'Cache Tab Information',
      height: 'Tab Bar Height',
      mode: {
        title: 'Tab Style',
        chrome: 'Chrome Style',
        button: 'Button Style'
      }
    },
    sider: {
      inverted: 'Dark Sidebar',
      width: 'Sidebar Width',
      collapsedWidth: 'Collapsed Sidebar Width',
      mixWidth: 'Mixed Layout Sidebar Width',
      mixCollapsedWidth: 'Mixed Layout Collapsed Sidebar Width',
      mixChildMenuWidth: 'Mixed Layout Sub-menu Width'
    },
    footer: {
      visible: 'Show Footer',
      fixed: 'Fixed Footer',
      height: 'Footer Height',
      right: 'Footer Right'
    },
    watermark: {
      visible: 'Show Full Screen Watermark',
      text: 'Watermark Text'
    },
    themeDrawerTitle: 'Theme Configuration',
    pageFunTitle: 'Page Functions',
    resetCacheStrategy: {
      title: 'Reset Cache Strategy',
      close: 'Close Page',
      refresh: 'Refresh Page'
    },
    configOperation: {
      copyConfig: 'Copy Configuration',
      copySuccessMsg: 'Copied successfully, please replace the variable themeSettings in src/theme/settings.ts',
      resetConfig: 'Reset Configuration',
      resetSuccessMsg: 'Reset successful'
    }
  },
  route: {
    login: 'Login',
    403: 'No Permission',
    404: 'Page Not Found',
    500: 'Server Error',
    'iframe-page': 'External Page',
    home: 'Home',
    'reset-password': 'Reset Password'
  },
  page: {
    login: {
      feature: {
        title: 'Sanic Admin',
        subtitle: 'Access all features and services with your account',
        dataAnalysis: 'Data Analysis',
        dataAnalysisDesc: 'Powerful data analysis features to help you make informed decisions',
        realTimeCollaboration: 'Real-time Collaboration',
        realTimeCollaborationDesc: 'Support for real-time online collaboration to improve team efficiency',
        strongSecurity: 'Strong Security',
        strongSecurityDesc: 'Advanced security technology to protect your data',
        customerSupport: 'Customer Support',
        customerSupportDesc: '24/7 professional customer service support'
      },
      common: {
        agreeToTermsError: 'Please agree to the terms of service',
        accountPlaceholder: 'Please enter username/email/phone',
        codeAccountPlaceholder: 'Please enter email/phone',
        userNamePlaceholder: 'Please enter username',
        phonePlaceholder: 'Please enter phone number',
        codePlaceholder: 'Please enter verification code',
        passwordPlaceholder: 'Please enter password',
        confirmPasswordPlaceholder: 'Please confirm password',
        verificationCodePlaceholder: 'Please enter verification code',
        captchaPlaceholder: 'Please enter captcha code',
        getCode: 'Get Code',
        loginSuccess: 'Login Successful',
        registerSuccess: 'Registration Successful',
        agreeToTerms: 'I have read and agree to the',
        termsLink: 'Terms of Service',
        rememberMe: 'Remember me'
      },
      login: {
        title: 'Welcome Back',
        subtitle: 'Access all features and services with your account',
        loginText: 'Login',
        passwordLogin: 'Password Login',
        verificationCodeLogin: 'Verification Code Login',
        forgetPassword: 'Forgot Password?',
        or: 'Or'
      },
      register: {
        title: 'Register Account',
        subtitle: 'Register an account with your email',
        back: 'Back'
      }
    },
    home: {
      branchDesc:
        'To facilitate development and updates, we have simplified the main branch code, keeping only the home menu. The remaining content has been moved to the example branch for maintenance. The preview address shows the content of the example branch.',
      greeting: 'Good morning, {userName}, another energetic day!',
      weatherDesc: 'Today will be cloudy turning clear, 20℃ - 25℃!',
      projectCount: 'Projects',
      todo: 'Todo',
      message: 'Messages',
      downloadCount: 'Downloads',
      registerCount: 'Registrations',
      schedule: 'Schedule',
      study: 'Study',
      work: 'Work',
      rest: 'Rest',
      entertainment: 'Entertainment',
      visitCount: 'Visits',
      turnover: 'Turnover',
      dealCount: 'Deals',
      projectNews: {
        title: 'Project Updates',
        moreNews: 'More Updates',
        desc1: 'Soybean created the open-source project soybean-admin on May 28, 2021!',
        desc2: 'Yanbowe submitted a bug to soybean-admin: multi-tab bar not auto-adapting.',
        desc3: 'Soybean is preparing thoroughly for the release of soybean-admin!',
        desc4: 'Soybean is busy writing project documentation for soybean-admin!',
        desc5: 'Soybean just quickly put together the workbench page, it looks decent enough!'
      },
      creativity: 'Creativity'
    },
    resetPassword: {
      waitToResend: "Haven't received the code? Can resend after {countdown}s",
      title: 'Reset Password',
      verifyEmail: 'Verify Email',
      enterCode: 'Enter Code',
      setPassword: 'Set New Password',
      codeSentTo: 'Verification code sent to',
      submit: 'Confirm Reset',
      success: 'Password Reset Successful',
      failed: 'Password Reset Failed',
      newPassword: 'New Password',
      confirmPassword: 'Confirm Password',
      resendCode: 'Resend Verification Code',
      resendCodeDesc: 'We will resend a verification code to {email}. Please confirm your email address is correct.'
    }
  },
  form: {
    required: 'Required',
    userName: {
      required: 'Please enter username',
      invalid: 'Invalid username format'
    },
    phone: {
      required: 'Please enter phone number',
      invalid: 'Invalid phone number format'
    },
    pwd: {
      required: 'Please enter password',
      invalid: 'Invalid password format, 6-18 characters, including letters, numbers, and underscores'
    },
    confirmPwd: {
      required: 'Please confirm password',
      invalid: 'Passwords do not match'
    },
    code: {
      required: 'Please enter verification code',
      invalid: 'Invalid verification code format'
    },
    email: {
      required: 'Please enter email',
      invalid: 'Invalid email format'
    }
  },
  dropdown: {
    closeCurrent: 'Close Current',
    closeOther: 'Close Others',
    closeLeft: 'Close Left',
    closeRight: 'Close Right',
    closeAll: 'Close All'
  },
  icon: {
    themeConfig: 'Theme Config',
    themeSchema: 'Theme Mode',
    lang: 'Switch Language',
    fullscreen: 'Fullscreen',
    fullscreenExit: 'Exit Fullscreen',
    reload: 'Refresh Page',
    collapse: 'Collapse Menu',
    expand: 'Expand Menu',
    pin: 'Pin',
    unpin: 'Unpin'
  },
  datatable: {
    itemCount: 'Total {total} items'
  }
};

export default local;
