// import ui from "@fast-crud/ui-naive";

// export function  {

//   app.use(FastCrud, {

//     commonOptions(props: UseCrudProps): CrudOptions {
//       console.log('commonOptions', props);
//       const currentPath = router.currentRoute.value.path
//       const opts: CrudOptions = {
//         table: {
//           size: "small",
//         },
//         container: {
//           is: shallowRef(CrudLayout),
//         },
//         form: {
//           labelWidth: "100px",
//           group: {
//             display: "grid",
//           },
//         },
//         columns: {
//           _index: {
//             title: "序号",
//             form: { show: false },
//             column: {
//               order: -9999,
//               align: "center",
//               width: "55px",
//               fixed: "left",
//               columnSetDisabled: true, //禁止在列设置中选择
//               formatter: (context: any) => {
//                 //计算序号,你可以自定义计算规则，此处为翻页累加
//                 let index = context.index ?? 1;
//                 let pagination: any = props.crudExpose.crudBinding.value.pagination;
//                 return ((pagination.page ?? 1) - 1) * pagination.pageSize + index + 1;
//               },
//             },
//           },
//         },
//         rowHandle: {
//           fixed: "right",
//           show: hasAnyButtonPermission([`${currentPath}::row_remove`, `${currentPath}::row_view`, `${currentPath}::row_edit`]),
//           align: "center",
//           width: 160,
//           buttons: {
//             remove: {
//               icon: "ant-design:delete-outlined",
//               text: "",
//               show: hasButtonPermission(`${currentPath}::row_remove`),
//               size: 'small'
//             },
//             view: {
//               icon: "ant-design:eye-outlined",
//               text: "",
//               show: hasButtonPermission(`${currentPath}::row_view`),
//               size: 'small'
//             },
//             edit: {
//               icon: "ant-design:edit-outlined",
//               text: "",
//               show: hasButtonPermission(`${currentPath}::row_edit`),
//               size: 'small'
//             },
//           },
//         },
//         toolbar: {
//           buttons: {
//             search: { text: "", quaternary: true, type: "default", show: false },
//             refresh: { text: "", quaternary: true, type: "default", show: false },
//             compact: { text: "", quaternary: true, type: "default", show: false },
//             export: { text: "", quaternary: true, type: "default", show: false, },
//             columns: { quaternary: true, type: "default", show: hasButtonPermission(`${currentPath}::col_setting`) },
//           }
//         },
//         actionbar: {
//           buttons: {
//             add: {
//               show: hasButtonPermission(`${currentPath}::create`),
//               icon: "ant-design:plus-outlined",
//               text: "新增数据",
//             },
//           },
//         }
//       }
//       return opts;
//     }
//   } as FsSetupOptions);

//   app.use(FsExtendsUploader, {
//     defaultType: "form",
//     form: {
//       keepName: true,
//       action: `${import.meta.env.VITE_SERVICE_BASE_URL}/accessory/local/upload`,
//       name: "files",
//       withCredentials: false,
//       uploadRequest: async ({ action, file, onProgress }: any) => {
//         // @ts-ignore
//         const data = new FormData();
//         data.append("file", file);
//         console.log("自定义表单文件上传请求", file);
//         return await request({
//           url: action,
//           method: "post",
//           headers: {
//             "Content-Type": "multipart/form-data",
//           },
//           timeout: 60000,
//           data,
//           onUploadProgress(progress: any) {
//             onProgress({
//               percent: Math.round((progress.loaded / progress.total) * 100),
//             });
//           },
//         });
//       },
//       successHandle(ret: any) {
//         // 上传完成后的结果处理， 此处应返回格式为{url:xxx}
//         return {
//           url: ret.data[0].url,
//           key: ret.data[0].id,
//         };
//       },
//     },
//   });
// }
